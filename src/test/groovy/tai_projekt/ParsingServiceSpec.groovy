package tai_projekt

import grails.test.mixin.TestFor
import spock.lang.Specification

import java.text.ParseException

/**
 * Created by Kisiel on 05.07.2017.
 */
@TestFor(ParsingService)
class ParsingServiceSpec extends Specification {

    def service

    def "test parsing Date"(){
        given:
        service = new ParsingService()

        when:
        service.parseDate("aaaa")

        then:
        thrown(ParseException.class)

        when:
        service.parseDate("2017-07-05T17:00:00+0000")

        then:
        noExceptionThrown()
    }
}
