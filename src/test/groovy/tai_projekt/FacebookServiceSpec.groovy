package tai_projekt

import grails.plugin.facebooksdk.FacebookGraphClient
import grails.test.mixin.TestFor
import grails.test.mixin.TestMixin
import grails.test.mixin.domain.DomainClassUnitTestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import org.junit.Test
import spock.lang.Specification

/**
 * Created by Kisiel on 05.07.2017.
 */
@TestFor(FacebookService)
@TestMixin(GrailsUnitTestMixin)
class FacebookServiceSpec extends Specification {

    def facebookContext = [:]
    def client

    private static createUser(){
        AppUser user = new AppUser()
        user.facebookID = "123"
        user.firstLoginDate = new Date()
        user.lastLoginDate = new Date()
        user
    }

    def setup() {
        def user = createUser()
        def facebookGraphClient = Spy(FacebookGraphClient){
            fetchObject(_) >> user
            fetchConnection(_) >> [[permission:'user_likes', status:'granted']]
            fetchConnection(_, _) >> [[createdTime: "2017-07-05T17:00:00+0000"]]
        }
        service.appUserService = Spy(AppUserService){
            updateUserData(_) >> user
        }
        service.parsingService = Spy(ParsingService){
            parseDate(_) >> "data"
        }

        service.fanpageService = Spy(FanpageService){
            createNewFanPageInstance(_, _) >> true
        }
        service.facebookGraphClientService = Spy(FacebookGraphClientService){
            createFacebookGraphClient(_) >> facebookGraphClient
        }
        facebookContext.authenticated = true
        facebookContext.user = [:]
        facebookContext.user.token = "aa"
        client = Spy(FacebookGraphClientService.class){
            createFacebookGraphClient(_) >> null
        }
    }

    def cleanup() {
    }

    void "test getUserData"(){
        given:
        def res = service.getUserData(facebookContext)

        expect:
        res.user == res.appUser
        res.user != null
    }

    void "test getFanpages"(){
        given:
        def res = service.getFanpages(facebookContext, 10, 10)

        expect:
        res.permissions != null
        res.user == res.appUser
        res.user != null
        res.listSize == 1
        res.error ==  null
        res.max == 10
    }
}

