package tai_projekt

import grails.transaction.Transactional

@Transactional
class FanpageService {

    def createNewFanPageInstance(params, user){
        Fanpage fanpage = new Fanpage()
        fanpage.user = user
        fanpage.name = params.name
        fanpage.likeDate = params.created_time
        fanpage.facebookFanpageId = params.id
        fanpage.save(failOnError:true, flush:true)

    }
}
