package tai_projekt


import java.text.SimpleDateFormat

class ParsingService {

    def parseDate(String date){
        new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZ").parse(date)
    }
}
