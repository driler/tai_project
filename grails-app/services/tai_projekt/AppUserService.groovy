package tai_projekt

import grails.transaction.Transactional

import java.time.LocalDate

@Transactional
class AppUserService {

    def saveUser(AppUser user){
        user.save(flush:true, failOnError:true)
    }

    def updateUserData(String id){
        def user = AppUser.findByFacebookID(id)
        if(user == null){
            user = new AppUser()
            user.facebookID = id
            user.firstLoginDate = new Date()
        }
        user.lastLoginDate = new Date()

        saveUser(user)
    }
}
