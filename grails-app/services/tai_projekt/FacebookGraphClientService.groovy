package tai_projekt

import grails.plugin.facebooksdk.FacebookGraphClient
import grails.transaction.Transactional

@Transactional
class FacebookGraphClientService {

    def createFacebookGraphClient(String token){
        new FacebookGraphClient(token)
    }
}
