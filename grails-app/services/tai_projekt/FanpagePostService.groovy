package tai_projekt

import grails.transaction.Transactional

@Transactional
class FanpagePostService {

    ParsingService parsingService

    def createFanPagePostInstance(params, fanpage){
        FanpagePost fanpagePost = new FanpagePost()
        fanpagePost.page = fanpage
        fanpagePost.date = parsingService.parseDate(params.created_time)
        fanpagePost.content = params.message?:(params.story?:("<some other action on facebook>"))
        fanpagePost.facebookId = params.id

        fanpagePost.save(flush:true, failOnError:true)

    }
}
