package tai_projekt

import com.restfb.exception.FacebookOAuthException
import grails.transaction.Transactional
import org.grails.web.json.JSONObject

@Transactional
class FacebookService {
    def facebookGraphClient

    AppUserService appUserService
    FanpageService fanpageService
    ParsingService parsingService
    FanpagePostService fanpagePostService
    FacebookGraphClientService facebookGraphClientService


    def getUserData(facebookContext){
        def user
        def appUser
        List permissions = []
        def error
        if (facebookContext.authenticated) {
            String token = facebookContext.user.token
            if (token) {
                facebookGraphClient = facebookGraphClientService.createFacebookGraphClient(token)
                try {
                    user = facebookGraphClient.fetchObject(facebookContext.user.id as String)
                    appUser = appUserService.updateUserData(user.id)
                } catch (FacebookOAuthException exception) {
                    exception.printStackTrace()
                    error = "Got exception: ${exception.getMessage()}"
                    facebookContext.user.invalidate()
                }
            } else
                error = "Cannot generate token for user"

        }else
           error = "Cannot authenticate user"

        [ permissions: permissions, user: user, appUser: appUser, error: error]
    }

    def getFanpages(facebookContext, int limit, int offset){
        def user
        def appUser
        def error
        List permissions = []
        List userLikes = []

        if (facebookContext.authenticated) {
            String token = facebookContext.user.token
            if (token) {
                facebookGraphClient = facebookGraphClientService.createFacebookGraphClient(token)
                try {
                    user = facebookGraphClient.fetchObject(facebookContext.user.id as String)
                    appUser = appUserService.updateUserData(user.id)
                    permissions = facebookGraphClient.fetchConnection("${facebookContext.user.id}/permissions")
                    if (permissions.find { it.permission == 'user_likes' && it.status == 'granted' } ) {
                        userLikes = facebookGraphClient.fetchConnection("${facebookContext.user.id}/likes", [limit:limit, offset: limit*offset])
                        userLikes.each {
                            it.created_time = parsingService.parseDate(it.created_time)
                            fanpageService.createNewFanPageInstance(it, appUser)
                        }
                    }else
                        error = "Cannot get fanpages details"

                } catch (FacebookOAuthException exception) {
                    exception.printStackTrace()
                    error = "Got exception: ${exception.getMessage()}"
                    facebookContext.user.invalidate()
                }
            } else
                error =  "Cannot generate token for user"

        }else
            error = "Cannot authenticate user"

        [ permissions: permissions, appUser: appUser, user: user, userLikes: userLikes, listSize:userLikes.size(), error: error, max: 10]
    }


    def getFanpagesWithPosts(facebookContext){
        def error
        def pagesWithLikes = [:]
        if (facebookContext.authenticated) {
            String token = facebookContext.user.token
            if (token) {
                facebookGraphClient = facebookGraphClientService.createFacebookGraphClient(token)
                try {
                    if(Fanpage.count == 0)
                        getFanpages(facebookContext, 20, 0)

                    Fanpage.list().each { page ->
                        pagesWithLikes = facebookGraphClient.fetchObject("${page.facebookFanpageId}/posts")
                        pagesWithLikes.each{ post ->
                            post.value.each{ aa ->
                                if(aa instanceof JSONObject)
                                    fanpagePostService.createFanPagePostInstance(aa, page)
                            }
                        }
                    }

                    pagesWithLikes.newestPost =  FanpagePost.createCriteria().list {
                        order('date', 'desc')
                        maxResults(1)
                    }

                    pagesWithLikes.oldestPost =  FanpagePost.createCriteria().list {
                        order('date', 'asc')
                        maxResults(1)
                    }

                } catch (FacebookOAuthException exception) {
                    exception.printStackTrace()
                    error = "Got exception: ${exception.getMessage()}"
                    facebookContext.user.invalidate()
                }
            } else
                error =  "Cannot generate token for user"

        }else
            error = "Cannot authenticate user"



        [pages:pagesWithLikes, error: error, max: 10]
    }


    def logout(facebookContext){
        try{
            facebookContext.user.invalidate()
            true
        }catch (Exception ignored){
            flash.error = "cannot logout user"
        }
    }



}
