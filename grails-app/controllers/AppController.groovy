import grails.plugin.facebooksdk.FacebookContext
import tai_projekt.FacebookService

class AppController {

    static defaultAction = 'index'
    static transactional = false

    FacebookService facebookService
    FacebookContext facebookContext

    def beforeInterceptor = {
        log.info "START ${actionUri} with params=${params}"
    }
    def afterInterceptor = {
        log.info "END ${actionUri}"
    }


	def index() {
        def map = facebookService.getUserData(facebookContext)
        map.error? (flash.error=map.error) : (flash.message="Got user data")
        map.facebookContext =  facebookContext
        map
	}

    def showFanpages(){
        def limit = params.numberOfPages? params.numberOfPages as Integer:10
        def offset = params.currentPage? params.currentPage as Integer:0
        offset = params.Next? (offset +1) : (params.Previous?(offset-1): offset)

        genParams(limit, offset)
    }

    def showStatistics(){
        facebookService.getFanpagesWithPosts(facebookContext)
    }


    def logout(){
        if(facebookService.logout(facebookContext))
            redirect view: 'index'
        else {
            flash.error = "Cannot invalidate user"
            redirect view: 'index'
        }
    }

    private genParams(limit, offset){

        def map = facebookService.getFanpages(facebookContext, limit, offset)
        map.error ? (flash.error = map.error) : (flash.message = "Got fanpages")
        map.facebookContext = facebookContext
        map.totalnumber = params.max? (params.max as Integer):(params.totalNumber?:null)
        map.currentPage = offset
        map.end = limit * offset <  (map.totalNumber as Integer)

        if(params.numberOfPages)
            map.numberOfPages = params.numberOfPages as Integer
        map
    }




}
