beans = {
    facebookService = ref("FacebookService")

    facebookCookieService = ref("FacebookCookieService")

    cookieService = ref("CookieService")

    appUserService = ref("AppUserService")
}