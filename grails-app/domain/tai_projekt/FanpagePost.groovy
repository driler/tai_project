package tai_projekt

class FanpagePost {

    String content
    Date date
    String facebookId

    static belongsTo = [page: Fanpage]

    static constraints = {
        content maxSize: 5000
    }

}
