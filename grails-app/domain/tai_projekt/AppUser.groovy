package tai_projekt

import org.apache.tomcat.jni.Local

import java.time.LocalDate

class AppUser {

    String facebookID
    Date firstLoginDate
    Date lastLoginDate

    static constraints = {
        id unique: true

    }
}
