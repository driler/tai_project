<html>
<head>
    <meta name="layout" content="main"/>
    <script type="text/javascript" src="//connect.facebook.net/en_US/sdk.js"></script>
    <title>FacebookFilter</title>
</head>
<body>

<div id="content" role="main">
    <section class="row colset-2-its">

        <g:form controller="app" action="index">
            <g:submitButton name="index" value="${message(code:'label.index')}"/>
        </g:form>

        <g:if test="${pages}">
            <h2><p>
                The newest saved post: ${pages.newestPost.content}
                <br/>This post is from page: ${pages.newestPost.page.name}
            </p></h2>

            <h2><p>
                The newest saved post:${pages.oldestPost.content}
                <br/>This post is from page: ${pages.oldestPost.page.name}
            </p></h2>
        </g:if>
    </section>

</div>
<div id="posts">
    <section class="row colset-2-its">
        <div id="postsss">

        </div>
    </section>
</div>
</body>
</html>