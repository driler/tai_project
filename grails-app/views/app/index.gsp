<html>
<head>
	<meta name="layout" content="main"/>
	<title>FacebookFilter</title>
</head>
<body>


	<div id="content" role="main" style="width: 100%">
		<section class="row colset-2-its" >
			<g:if test="${!facebookContext.app.id}">
				<g:render template="/configError" />
			</g:if>
			<g:else>
				<facebook:initJS
						appId="${facebookContext.app.id}">
				</facebook:initJS>

				<g:if test="${!facebookContext.authenticated}">
					<g:render template="/loginRedirect"/>
				</g:if>
				<g:else>
					<g:render template="/userInfo" model="[user: user]"/>

					<g:form controller="app" action="showFanpages">
						<g:submitButton name="showFanpages" value="${message(code:'label.show.user.fanpages')}"/>
					</g:form>


					<g:form controller="app" action="showStatistics">
						<g:submitButton name="showStatistics" value="${message(code:'label.show.statistics')}"/>
					</g:form>

					<g:form controller="app" action="logout">
					    <g:submitButton name="logout" value="${message(code:'label.logout')}"/>
					</g:form>
				</g:else>
			</g:else>
		</section>
	</div>
</body>
</html>