<html>
<head>
    <meta name="layout" content="main"/>
    <script type="text/javascript" src="//connect.facebook.net/en_US/sdk.js"></script>
    <title>FacebookFilter</title>
</head>
<body>
<script>

    function statusChangeCallback(response) {
        if (response.status === 'connected') {
            testAPI();
        }
        else {
            alert("not connected, not logged into facebook, we don't know");
        }
    }

    function checkLoginState() {
        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });
    }

    function showPosts(like){
        FB.api(
            "/"+like.id+"/posts",
            {limit:10, summary:true},
            function (response) {
                if (response && !response.error) {
                    document.getElementById("postsss").innerHTML = "";

                    $('#postsss').append("<h3>The latest 10 posts from <b>" + like.name + "</b></h3>");

                    var ul = $('<ul>').appendTo('#postsss');

                    $(response.data).each(function(index, item) {
                        var myDate = new Date(item.created_time);
                        var date = (myDate.getMonth() + 1) + "-" + myDate.getDate() + "-" + myDate.getFullYear();
                        date += " " + myDate.getHours() + ":" + myDate.getMinutes() + ":" + myDate.getSeconds();
                        if(item.story) {
                            ul.append(
                                $(document.createElement('li')).text(date + "  {added new photo, not implemented yet}")
                            );
                        }else{
                            ul.append(
                                $(document.createElement('li')).text(date + "   " + item.message)
                            );
                        }
                    });
                }
                else {
                    console.log(response.error)
                }
            }

        );
    }

    function testAPI() {
        FB.api(
            "/${user.id}/likes",
            {limit:10, summary:true},
            function (response) {
                if (response && !response.error) {
                    console.log(response);
                    document.getElementById("max").setAttribute("value",response.summary.total_count )
                }
                else {
                    console.log(response.error)
                }
            }
        );
    }

    $(document).ready(function() {
        FB.init({
            appId: "${facebookContext.app.id}",
            version: "v2.7",  // API version
            channelUrl: "http://localhost:8080/facebookSdk/channel?locale=pl_PL",  // Custom channel URL
            cookie: true, // enable cookies to allow the server to access the session
            oauth: true, // enables OAuth 2.0
            status: false, // check login status
            xfbml: false, // parse XFBML
            frictionlessRequests: false // to enable frictionless requests
        });

        checkLoginState();
    });
</script>
<div id="content" role="main">
    <section class="row colset-2-its">

        <g:form controller="app" action="index">
            <g:submitButton name="index" value="${message(code:'label.index')}"/>
        </g:form>


        <g:if test="${!totalnumber}">
            <h1>Top ${listSize} newest fanpages you like:</h1>
        </g:if>
        <g:else>
            <h1>More pages:</h1>
        </g:else>
        <small>Click name to see posts!</small>
        <ul>
            <g:each in="${userLikes}" var="like">
                <li>
                    <p onclick="showPosts(${like})"> <g:formatDate format="yyyy-MM-dd  hh:mm:ss" date="${like.created_time}"/>  ${like.name} </p>
                </li>
            </g:each>
        </ul>
        <g:if test="${totalnumber}">
            <div class="pagination">
            <g:form action="showFanpages">
                <g:if test="${currentPage}">
                    <input id="max" type="hidden" name="max" value="${max}"/>
                    <input type="hidden" name="currentPage" value="${currentPage}"/>
                    <g:submitButton name="Previous" value="${message(code:'label.previous.button')}"/>
                </g:if>
                <input type="hidden" name="totalNumber" value="${totalnumber}"/>
                <input type="hidden" name="numberOfPages" value="${numberOfPages}">
                <g:if test="${!end}">
                    <g:submitButton name="Next" value="${message(code:'label.next.button')}"/>
                </g:if>
            </g:form>
            </div>
            <h4>You see ${(currentPage) * numberOfPages} - ${(currentPage+1) * numberOfPages} pages</h4>
        </g:if>

        <h3>Want to see more? Just insert number how many and press OK</h3>
            <g:form action="showFanpages">
                <input id="max" type="hidden" name="max" value="10"/>
                <input type="number" name="numberOfPages" min="1" value="10">
                <g:submitButton name="showPages" value="${message(code:'label.ok.button')}"/>
            </g:form>
        <br>
        <g:if test="${totalnumber}">
            <h4>Total number of pages you like: ${totalnumber}</h4>
        </g:if>
    </section>

</div>
<div id="posts">
    <section class="row colset-2-its">
        <div id="postsss">

        </div>
    </section>
</div>
</body>
</html>