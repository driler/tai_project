<p style="color:  #0f0f0f;">
    <facebook:picture
            facebookId="${user.id}"
            linkEnabled="false" />
<h1><b>${user.name}</b></h1>

    <h3><b>First login date:</b> <g:formatDate format="yyyy-MM-dd hh:mm:ss" date="${appUser.firstLoginDate}"/></h3>
    <h3><b>Last login date:</b>  <g:formatDate format="yyyy-MM-dd hh:mm:ss" date="${appUser.lastLoginDate}"/></h3>
</p>
