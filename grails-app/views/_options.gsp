<g:render template="/userInfo" model="[user: user]"/>

<g:form controller="app" action="showFanpages">
  <g:submitButton name="showFanpages" value="${message(code:'label.show.user.fanpages')}"/>
</g:form>

<g:form controller="app" action="getPagesPost">
    <g:submitButton name="posts" value="${message(code:'label.show.fanpages.post')}"/>
</g:form>

<g:form controller="App" action="logout">
  <g:submitButton name="logout" value="${message(code: 'label.logout')}" />
</g:form>