<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>FacebookFilter</title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
</head>
<body>

    <div id="content" role="main" style="width: 100%">
        <section class="row colset-2-its">
            <h1>Welcome to FacebookFilter</h1>
            <h2>This app allows you to show content only from fanpages you like on Facebook</h2>

            <p>All you have to do is login using your Facebook account and choose option </p>
            <div id="controllers" role="navigation">
                <ul>
                    <li class="controller">
                        <g:link controller="App" action="index">Login Page</g:link>
                    </li>
                </ul>
            </div>
        </section>
    </div>

</body>
</html>
