<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="FacebookFilter"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <asset:stylesheet src="application.css"/>

    <g:layoutHead/>
</head>
<body>
    <div class="svg" role="presentation">
        <div class="grails-logo-container">
            <a href="/">
            <asset:image src="logo.png"/>
            </a>
        </div>
    </div>

    <g:layoutBody/>

    <div class="footer" role="contentinfo"></div>

    <div id="spinner" class="spinner" style="display:none;">
        <g:message code="spinner.alt" default="Loading&hellip;"/>
    </div>



    <asset:javascript src="application.js"/>

</body>
</html>
